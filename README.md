# cluedo4KG

This repository contains the open-source educational resources designed for hands-on learning on knowledge graph technologies. Inspired by the board game Cluedo, these resources challenge learners to act as investigators solving a murder.

Two tutorials are available:
- SPARQLuedo
- OWLuedo

Both of them are implemented via a Web application (see Web app folder).

Specific ressources created for each of the tutorials are in dedicated folders.



## License
Cluedo4KG is developped by Camille Pradel, William Charles, Nathalie Hernandez is licensed under CC BY-NC 4.0

