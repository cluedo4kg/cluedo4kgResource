let questions = ["chargement des questions"]; 
let turtleArea;
let sparqlResult;
let sparqlEditor;
let currentQuestion = 0;
let apresP1 = false;

async function getQuestions(lang = document.querySelector("#lang").textContent) {
	const quest = await fetch(`./questions-${lang}.json`);
	return await quest.json();
}
	
getQuestions().then((questions) => {
	document.querySelector("#commencer").addEventListener('click', () => {
		document.querySelector('#hero1').innerHTML = '<em>' + (document.querySelector('#binome1').value? document.querySelector('#binome1').value:"Bibi")+ '</em>';
		document.querySelector('#hero2').innerHTML = '<em>' + (document.querySelector('#binome2').value?document.querySelector('#binome2').value:"Bibo") + '</em>';
		document.querySelector('#consignes').style.display = 'none';
		document.querySelector('#enquete').style.display = 'block';
		document.querySelector('body').style.background = "url('media/ron_blood_032.png') repeat scroll 0 0 black";

		turtleArea = CodeMirror.fromTextArea(document.querySelector("#codeturtle"), {
			mode: "text/turtle",
			tabMode: "indent",
			matchBrackets: true,
			readOnly: "true"
		});
		sparqlEditor = CodeMirror.fromTextArea(document.querySelector("#codesparql"), {
			mode: "application/x-sparql-query",
			tabMode: "indent",
			matchBrackets: true,
		});
		sparqlResult = CodeMirror.fromTextArea(document.querySelector("#codexml"), {
			mode: { name: "javascript", json: true },
			matchBrackets: true,
			readOnly: "true"
		});

		updateQuestion(currentQuestion, questions);
	});
	document.querySelector('#executeSparqlButton').addEventListener('click', () => {
		
		executeSparql(sparqlEditor.getValue(), document.querySelector('#sparqlendpoint').value);
	});

	document.querySelector('#questpred').addEventListener('click', () => {
		if (currentQuestion > 0) {
			currentQuestion--;
			updateQuestion(currentQuestion,questions,false);
		}
	});
	document.querySelector('#questsuiv').addEventListener('click', () => {
		if (currentQuestion < questions.length - 1) {
			currentQuestion++;
			updateQuestion(currentQuestion,questions,true);
		}
	});
});
	

	//$(document).mousemove(function(e){
	/*document.querySelector('document').addEventListener('mouseover',() =>{
		$('body').css('background-position', '-'+e.pageX/10+'px -'+e.pageY/10+'px');
	  });
  */

/*});*/

function updateQuestion(currentQuestion,questions,avance) {

	document.querySelector('#numquestion').textContent = currentQuestion + 1;
	if (questions[currentQuestion]["partie"]) {
		apresP1 = true;
		document.querySelector('#partie').innerHTML = questions[currentQuestion]["partie"];
		document.querySelector('#info').innerHTML = questions[currentQuestion]["info"];
		document.querySelector('#question').innerHTML = questions[currentQuestion]["question"];
		document.querySelector('#sparqlquery').style.display = 'none';
		document.querySelector('#executeSparqlButton').style.display = 'none';
		document.querySelector('#ontology').style.display = 'none';
		document.querySelector('#response').style.display = 'none';

		
	}
	else {
		
		if (document.querySelector('#sparqlquery').style.display === 'none') {
			document.querySelector('#sparqlquery').style.display = 'block';
			document.querySelector('#executeSparqlButton').style.display = 'block';
			document.querySelector('#ontology').style.display = 'inline-block';
			document.querySelector('#response').style.display = 'block';
			//sparqlEditor.setValue('SELECT distinct ?a \nWHERE {\nSERVICE <http://dbpedia.org/sparql> { ?a ?b ?c}\n LIMIT 2\n} ');
			if (apresP1 && !avance) document.querySelector('#partie').innerHTML = "";
		}
		
		
		document.querySelector('#info').innerHTML = questions[currentQuestion]["info"];
		document.querySelector('#question').innerHTML = questions[currentQuestion]["question"];
		turtleArea.setValue(questions[currentQuestion]["ontology"])

		document.querySelector('#questpred').style.color = (currentQuestion == 0 ? '#555' : 'white');
		document.querySelector('#questsuiv').style.color = (currentQuestion == questions.length - 1 ? '#555' : 'white');
	}
}


function executeSparql(sparqlQuery, sparqlServerUrl) {
	sparqlResult.setValue("Requête en cours...");
	console.log(sparqlQuery);

	/*fetch(`${sparqlServerUrl}?${new URLSearchParams({
		query: sparqlQuery
	})}`,*/
	fetch (sparqlServerUrl,
	{
		method: "POST", // *GET, POST, PUT, DELETE, etc.
		//cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
 
		headers: {
			//'Content-Type': "application/json",
			'Content-Type': 'application/x-www-form-urlencoded',
			//'Accept': 'application/sparql-results+json'
		},
		body: `query=${sparqlQuery}`/*new URLSearchParams({
			query: sparqlQuery
		})
		/*body: {
			query : sparqlQuery
		}*/
	})	
		
		.then(response => { 
			console.log(response);
		if (! response.ok) alert("Problem with the SPARQL endpoint!");
		return response.text();
		})
		.then(response => {
			console.log(response);
			sparqlResult.setValue(response);
		})
		.catch(err => {
	
			alert("Problem with the query or the SPARQL endpoint!");
			sparqlResult.setValue(err);
		});
        
		
}
