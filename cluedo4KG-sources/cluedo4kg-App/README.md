# cluedo4KG-app
The code developed uses HTML, CCS and javascript.

The only library used in codemirror https://codemirror.net/ for displaying sparql queries and turtle ontology abstracts.

The code is configurated to used our SPARQL endpoint.
When used on a other SPARQL server, the new endpoint must be indicated in the index.html file and the files provided in cluedo4KG-SPARQLuedo-resources folder must be loaded on the SPARQL server.