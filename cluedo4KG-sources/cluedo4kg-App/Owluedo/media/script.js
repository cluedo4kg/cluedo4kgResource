let turtleArea;
let sparqlResult;
let sparqlEditor;
let currentQuestion = 0;
let prevPart = "";
let pass = false;

async function getQuestions(lang = document.querySelector("#lang").textContent) {
	const quest = await fetch(`./questionsOWL-${lang}.json`);
	return await quest.json();
}
	
getQuestions().then((questions) => {
	document.querySelector("#commencer").addEventListener('click', () => {
		document.querySelector('#hero1').innerHTML = '<em>' + (document.querySelector('#binome1').value? document.querySelector('#binome1').value:"Bibi")+ '</em>';
		document.querySelector('#hero2').innerHTML = '<em>' + (document.querySelector('#binome2').value?document.querySelector('#binome2').value:"Bibo") + '</em>';
		document.querySelector('#consignes').style.display = 'none';
	document.querySelector('#enquete').style.display = 'block';
	document.querySelector('body').style.background = "url('media/ron_blood_032.png') repeat scroll 0 0 black";

	updateQuestion(currentQuestion,questions,true);
	});
	

	document.querySelector('#questpred').addEventListener('click', () => {
		if (currentQuestion > 0) {
			currentQuestion--;
			updateQuestion(currentQuestion,questions,false);
		}
	});
	document.querySelector('#questsuiv').addEventListener('click', () => {
		if (currentQuestion < questions.length - 1) {
			currentQuestion++;
			updateQuestion(currentQuestion,questions,true);
		}
	});

	//$(document).mousemove(function(e){
	/*document.querySelector('document').addEventListener('mouseover',() =>{
		$('body').css('background-position', '-'+e.pageX/10+'px -'+e.pageY/10+'px');
	  });
  */

});

function updateQuestion(currentQuestion,questions,avance) {
	
	
	if (questions[currentQuestion]["partie"]) {
		prevPart = document.querySelector('#partie').innerHTML;
		pass = true;
		document.querySelector('#partie').innerHTML = questions[currentQuestion]["partie"];
	}
	else {if (pass && !avance) document.querySelector('#partie').innerHTML = prevPart;
		pass = false;
	}
	
	
			
	document.querySelector('#numquestion').textContent = currentQuestion + 1;
	document.querySelector('#info').innerHTML = questions[currentQuestion]["info"];
	document.querySelector('#question').innerHTML = questions[currentQuestion]["question"];
	document.querySelector('#ontology').innerHTML = (questions[currentQuestion]["image"] ? `<img src="${questions[currentQuestion]["image"]}" alt="protege"` : "");
		
	
}


