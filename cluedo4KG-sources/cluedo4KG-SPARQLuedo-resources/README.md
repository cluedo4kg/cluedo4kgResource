The cluedo4KG-onto.owl contains the turtle serialization of the ontology used in the cluedo4KG-SPARQLuedo tutorial. The ontology is also available online https://w3id.org/cluedo4KG/onto.

The cluedo4KG-KG-pasSat.owl contains the turtle serialization of the knowledge graph describing the murder scene. It is also available from our SPARQL endpoint https://w3id.org/cluedo4KG/KG.
